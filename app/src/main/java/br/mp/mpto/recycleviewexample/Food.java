package br.mp.mpto.recycleviewexample;

public class Food {
    private float rating;
    private String name;
    private int image;

    public Food(float rating, String name, int image) {
        this.rating = rating;
        this.name = name;
        this.image = image;
    }

    public float getRating() {
        return rating;
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }
}
