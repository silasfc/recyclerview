package br.mp.mpto.recycleviewexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

public class Main extends AppCompatActivity {

    ArrayList<Food> dataSet = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataSet = new ArrayList<Food>();
        dataSet.add(new Food(2, "California", R.drawable.california));

        dataSet = new ArrayList<Food>();
        dataSet.add(new Food(3, "Mussarela", R.drawable.mussarela));

        dataSet = new ArrayList<Food>();
        dataSet.add(new Food(4, "Mussarela", R.drawable.portuguesa));

        dataSet = new ArrayList<Food>();
        dataSet.add(new Food(5, "Mussarela", R.drawable.quatro_queijos));

        dataSet = new ArrayList<Food>();
        dataSet.add(new Food(6, "Mussarela", R.drawable.salada_de_frutas));

        dataSet = new ArrayList<Food>();
        dataSet.add(new Food(7, "Mussarela", R.drawable.sorvete));

        dataSet = new ArrayList<Food>();
        dataSet.add(new Food(8, "Mussarela", R.drawable.sucos));
    }
}
